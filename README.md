# manifest

Mostly about static sources for the build products. Yhe *_deps entries are for anything not taken care of by main-bower-files

# openshift

private bitbucket dependencies are removed from dist/package.json and added to the .openshift/action_hooks/deploy script

# release

build/ and dist/ are ignored by default. build/ is transitory
